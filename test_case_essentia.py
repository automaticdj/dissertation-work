import os
import numpy as np

from essentia.standard import MonoLoader
from essentia.standard import Spectrum, Windowing
from essentia.standard import MelBands


loader = MonoLoader(filename='test_case.wav')

audio = loader().astype('single')

frame = audio[:10000]

w = Windowing(type='hann')
spectrum = Spectrum()  # FFT would return complex FFT, we only want magnitude
melbands = MelBands(numberBands=12)

mfcc = melbands(spectrum(w(frame)))
print('MFCC output is\n{}'.format(mfcc))
