# About
Auxiliary files and drafts used in my dissertation "Creating coherent cross-fades in an automatic DJ software tool using a machine learning approach".

See project repo [here](https://bitbucket.org/automaticdj/dj)

# Installing

- Download the repository files of the automatic DJ system [here](https://bitbucket.org/automaticdj/dj) and copy the content of `Application` directory to the `Application` directory in the root of this repository.

- Download the repository files of the DJ Mix Ground Truth Extractor [here](https://bitbucket.org/automaticdj/dj-mix-ground-truth-extractor) and copy the content to the directory `dj_mix_ground_truth_extractor`  in the root of this repository.

# Requirements

Python version: `2.7.15`

Requirements: `pip install -r requirements.txt`

# Copyright
Copyright 2019 Miroslav Kovalenko <MiroslavKovalenko@icloud.com> Auxiliary files and drafts used in my dissertation "Creating coherent cross-fades in an automatic DJ software tool using a machine learning approach" See [project repo](https://bitbucket.org/automaticdj/dj)..

# License

Released under AGPLv3 license. See the [COPYING.txt](COPYING.txt) file for details.